@extends('layouts.app')
@section('content')
	<section class="row">
		<section class="col-md-10 col-md-offset-1">
			{!! Form::open(['route' => 'aprendiz/search', 'method' => 'post', 'novalidate', 'class' => 'form-inline']) !!}
				<article class="form-group">
					<label for="exampleInputName2">BUSCAR POR NOMBRE</label>
					<input type="text" class="form-control" name = "nombre" >
					<button type="submit" class="btn btn-default glyphicon glyphicon-search"></button>
					<a href="{{ route('aprendiz.index') }}" class="btn btn-primary">MOSTRAR TODO</a>
					<a href="{{ route('aprendiz.create') }}" class="btn btn-primary">CREAR NUEVO REGISTRO </a>
				</article>
			{!! Form::close() !!}
			<article class="form-group">
				<table class="table table-bordered">
					<tr>
						<th>nombres del estudiante</th>
						<th>apellidos del estudiante</th>
						<th>documento de identidad</th>
						<th>edad</th>
						<th>curso</th>
						<th>correo</th>
					</tr>
					<tbody>

						@foreach($aprendizs as $aprendiz)
							<tr>
								<td>{{ $aprendiz->nombre}}</td>
								<td>{{ $aprendiz->apellido}}</td>
								<td>{{ $aprendiz->documento}}</td>
								<td>{{ $aprendiz->edad}}</td>
								<td>{{ $aprendiz->curso}}</td>
								<td>{{ $aprendiz->correo}}</td>
								<td>
									<a class="btn btn-primary btn-xs" href="{{ route('aprendiz.edit',['id' => $aprendiz->id] )}}" >EDITAR</a>
									<a class="btn btn-danger btn-xs" href="{{ route('aprendiz/destroy',['id' => $aprendiz->id] )}}">ARCHIVAR</a>
								</td>
							</tr>
						@endforeach

					</tbody>
				</table>
			</article>
		</section>
	</section>
@endsection
