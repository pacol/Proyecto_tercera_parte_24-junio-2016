@extends('layouts.app')
@section('content')
	<section class="row">
		<section class="col-md-10 col-md-offset-1">
				{!! Form::model($aprendiz, ['route' => 'aprendiz/update', 'method' => 'put', 'novalidate']) !!}
						{!! Form::hidden('id', $aprendiz->id) !!}
						<article class="form-group">
							{!! Form::label('nombre', 'nombre') !!}
							{!! Form::text('nombre', null, ['class' => 'form-control', 'required' => 'required']) !!}
						</article>
						<article class="form-group">
							{!! Form::label('apellido', 'apellido') !!}
							{!! Form::text('apellido', null, ['class' => 'form-control' , 'required' => 'required']) !!}
						</article>
						<article class="form-group">
							{!! Form::label('documento', 'documento') !!}
							{!! Form::text('documento', null, ['class' => 'form-control' , 'required' => 'required']) !!}
						</article>
						<article class="form-group">
							{!! Form::label('edad', 'edad') !!}
							{!! Form::text('edad', null, ['class' => 'form-control' , 'required' => 'required']) !!}
						</article>
						<article class="form-group">
							{!! Form::label('curso', 'curso') !!}
							{!! Form::text('curso', null, ['class' => 'form-control' , 'required' => 'required']) !!}
						</article>
						<article class="form-group">
							{!! Form::label('correo', 'correo') !!}
							{!! Form::text('correo', null, ['class' => 'form-control' , 'required' => 'required']) !!}
						</article>
						
						
						<article class="form-group">
							{!! Form::submit('Enviar', null, ['class' => 'btn btn-success']) !!}
						</article>
				 {!! Form::close() !!}
		</section>
	</section>
@endsection
