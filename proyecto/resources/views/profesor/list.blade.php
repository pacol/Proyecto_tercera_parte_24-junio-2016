@extends('layouts.app')
@section('content')
	<section class="row">
		<section class="col-md-10 col-md-offset-1">
			{!! Form::open(['route' => 'profesor/search', 'method' => 'post', 'novalidate', 'class' => 'form-inline']) !!}
				<article class="form-group">
					<label for="exampleInputName2">BUSCAR POR NOMBRE</label>
					<input type="text" class="form-control" name = "nombre" >
					<button type="submit" class="btn btn-default glyphicon glyphicon-search"></button>
					<a href="{{ route('profesor.index') }}" class="btn btn-primary">MOSTRAR TODO</a>
					<a href="{{ route('profesor.create') }}" class="btn btn-primary">CREAR NUEVO REGISTRO </a>
				</article>
			{!! Form::close() !!}
			<article class="form-group">
				<table class="table table-bordered">
					<tr>
						<th>nombres del profesor</th>
						<th>apellidos del profesor</th>
						<th>documento</th>
						<th>genero</th>
					</tr>
					<tbody>

						@foreach($profesors as $profesor)
							<tr>
								<td>{{ $profesor->nombre}}</td>
								<td>{{ $profesor->apellido}}</td>
								<td>{{ $profesor->documento}}</td>
								<td>{{ $profesor->genero}}</td>
								<td>
									<a class="btn btn-primary btn-xs" href="{{ route('profesor.edit',['id' => $profesor->id] )}}" >EDITAR</a>
									<a class="btn btn-danger btn-xs" href="{{ route('profesor/destroy',['id' => $profesor->id] )}}">ARCHIVAR</a>
								</td>
							</tr>
						@endforeach

					</tbody>
				</table>
			</article>
		</section>
	</section>
@endsection
