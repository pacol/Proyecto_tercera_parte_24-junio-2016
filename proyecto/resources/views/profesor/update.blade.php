@extends('layouts.app')
@section('content')
	<section class="row">
		<section class="col-md-10 col-md-offset-1">
				{!! Form::model($profesor, ['route' => 'profesor/update', 'method' => 'put', 'novalidate']) !!}
						{!! Form::hidden('id', $profesor->id) !!}
						<article class="form-group">
							{!! Form::label('nombre', 'nombre') !!}
							{!! Form::text('nombre', null, ['class' => 'form-control', 'required' => 'required']) !!}
						</article>
						<article class="form-group">
							{!! Form::label('apellido', 'apellido') !!}
							{!! Form::text('apellido', null, ['class' => 'form-control' , 'required' => 'required']) !!}
						</article>
						<article class="form-group">
							{!! Form::label('documento', 'documento') !!}
							{!! Form::text('documento', null, ['class' => 'form-control' , 'required' => 'required']) !!}
						</article>
						<article class="form-group">
							{!! Form::label('genero', 'genero') !!}
							{!! Form::text('genero', null, ['class' => 'form-control' , 'required' => 'required']) !!}
						</article>
						<article class="form-group">
							{!! Form::submit('Enviar', null, ['class' => 'btn btn-success']) !!}
						</article>
				 {!! Form::close() !!}
		</section>
	</section>
@endsection
