@extends('layouts.app')
@section('content')
<section class="container">
	<section class="row">
		<article class="col-md-10 col-md-offset-1">
			{!! Form::open(['route' => 'profesor.store', 'method' => 'post', 'novalidate']) !!}
				<section class="form-group">
					{!! Form::label('nombre', 'nombre') !!}
					{!! Form::text('nombre', null, ['id' => 'nombre','class' => 'form-control', 'required' => 'required']) !!}
				</section>
				<section class="form-group">
					{!! Form::label('apellido', 'apellido') !!}
					{!! Form::text('apellido', null,['class' => 'form-control', 'required' => 'required']) !!}
				</section>
				<section class="form-group">
					{!! Form::label('documento', 'documento') !!}
					{!! Form::text('documento', null,['class' => 'form-control', 'required' => 'required']) !!}
				</section>
				<section class="form-group">
					{!! Form::label('genero', 'genero') !!}
					{!! Form::text('genero', null,['class' => 'form-control', 'required' => 'required']) !!}
				</section>
				<section class="form-group">
					{!! Form::submit('Enviar', ['class' => 'btn btn-succes ' ] ) !!}
				</section>
			{!! Form::close() !!}
		</article>
	</section>
</section>
@endsection