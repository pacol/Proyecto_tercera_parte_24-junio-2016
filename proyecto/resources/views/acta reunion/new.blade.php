@extends('layouts.app')
@section('content')
<section class="container">
	<section class="row">
		<article class="col-md-10 col-md-offset-1">
			{!! Form::open(['route' => 'actareunion.store', 'method' => 'post', 'novalidate']) !!}
				<section class="form-group">
					{!! Form::label('fecha', 'fecha') !!}
					{!! Form::text('fecha', null, ['id' => 'nombre','class' => 'form-control', 'required' => 'required']) !!}
				</section>
				<section class="form-group">
					{!! Form::label('horainicio', 'horainicio') !!}
					{!! Form::text('horainicio', null,['class' => 'form-control', 'required' => 'required']) !!}
				</section>
				<section class="form-group">
					{!! Form::label('horafinal', 'horafinal') !!}
					{!! Form::text('horafinal', null,['class' => 'form-control', 'required' => 'required']) !!}
				</section>
				<section class="form-group">
					{!! Form::label('lugar', 'lugar') !!}
					{!! Form::text('lugar', null,['class' => 'form-control', 'required' => 'required']) !!}
				</section>
				<section class="form-group">
					{!! Form::label('proyecto', 'proyecto') !!}
					{!! Form::text('proyecto', null,['class' => 'form-control', 'required' => 'required']) !!}
				</section>
				<section class="form-group">
					{!! Form::label('asunto', 'asunto') !!}
					{!! Form::text('asunto', null,['class' => 'form-control', 'required' => 'required']) !!}
				</section>
				<section class="form-group">
					{!! Form::label('asistentes', 'asistentes') !!}
					{!! Form::text('asistentes', null,['class' => 'form-control', 'required' => 'required']) !!}
				</section>
				<section class="form-group">
					{!! Form::label('asistentesexternos', 'asistentesexternos') !!}
					{!! Form::text('asistentesexternos', null,['class' => 'form-control', 'required' => 'required']) !!}
				</section>
				<section class="form-group">
					{!! Form::label('elaboradopor', 'elaboradopor') !!}
					{!! Form::text('elaboradopor', null,['class' => 'form-control', 'required' => 'required']) !!}
				</section>
				<section class="form-group">
					{!! Form::label('proximareunion', 'proximareunion') !!}
					{!! Form::text('proximareunion', null,['class' => 'form-control', 'required' => 'required']) !!}
				</section>
				<section class="form-group">
					{!! Form::label('descripcionreunion', 'descripcionreunion') !!}
					{!! Form::text('descripcionreunion', null,['class' => 'form-control', 'required' => 'required']) !!}
				</section>
				<section class="form-group">
					{!! Form::submit('Enviar', ['class' => 'btn btn-succes ' ] ) !!}
				</section>
			{!! Form::close() !!}
		</article>
	</section>
</section>
@endsection