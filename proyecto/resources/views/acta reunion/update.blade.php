@extends('layouts.app')
@section('content')
	<section class="row">
		<section class="col-md-10 col-md-offset-1">
				{!! Form::model($actareunion, ['route' => 'actareunion/update', 'method' => 'put', 'novalidate']) !!}
						{!! Form::hidden('id', $actareunion->id) !!}
						<article class="form-group">
							{!! Form::label('fecha', 'fecha') !!}
							{!! Form::text('fecha', null, ['class' => 'form-control', 'required' => 'required']) !!}
						</article>
						<article class="form-group">
							{!! Form::label('horainicio', 'horainicio') !!}
							{!! Form::text('horainicio', null, ['class' => 'form-control' , 'required' => 'required']) !!}
						</article>
						<article class="form-group">
							{!! Form::label('horafinal', 'horafinal') !!}
							{!! Form::text('horafinal', null, ['class' => 'form-control' , 'required' => 'required']) !!}
						</article>
						<article class="form-group">
							{!! Form::label('lugar', 'lugar') !!}
							{!! Form::text('lugar', null, ['class' => 'form-control' , 'required' => 'required']) !!}
						</article>
						<article class="form-group">
							{!! Form::label('proyecto', 'proyecto') !!}
							{!! Form::text('proyecto', null, ['class' => 'form-control' , 'required' => 'required']) !!}
						</article>
						<article class="form-group">
							{!! Form::label('asistentes', 'asistentes') !!}
							{!! Form::text('asistentes', null, ['class' => 'form-control' , 'required' => 'required']) !!}
						</article>
						<article class="form-group">
							{!! Form::label('asistentesexternos', 'asistentesexternos') !!}
							{!! Form::text('asistentesexternos', null, ['class' => 'form-control' , 'required' => 'required']) !!}
						</article>
						<article class="form-group">
							{!! Form::label('elaboradopor', 'elaboradopor') !!}
							{!! Form::text('elaboradopor', null, ['class' => 'form-control' , 'required' => 'required']) !!}
						</article>
						<article class="form-group">
							{!! Form::label('proximareunion', 'proximareunion') !!}
							{!! Form::text('proximareunion', null, ['class' => 'form-control' , 'required' => 'required']) !!}
						</article>
						<article class="form-group">
							{!! Form::label('descripcionreunion', 'descripcionreunioon') !!}
							{!! Form::text('descripcionreunion', null, ['class' => 'form-control' , 'required' => 'required']) !!}
						</article>
						
						<article class="form-group">
							{!! Form::submit('Enviar', null, ['class' => 'btn btn-success']) !!}
						</article>
				 {!! Form::close() !!}
		</section>
	</section>
@endsection