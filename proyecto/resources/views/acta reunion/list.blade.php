@extends('layouts.app')
@section('content')
	<section class="row">
		<section class="col-md-10 col-md-offset-1">
			{!! Form::open(['route' => 'actareunion/search', 'method' => 'post', 'novalidate', 'class' => 'form-inline']) !!}
				<article class="form-group">
					<label for="exampleInputName2">BUSCAR POR NOMBRE</label>
					<input type="text" class="form-control" name = "fecha" >
					<button type="submit" class="btn btn-default glyphicon glyphicon-search"></button>
					<a href="{{ route('actareunion.index') }}" class="btn btn-primary">MOSTRAR TODO</a>
					<a href="{{ route('actareunion.create') }}" class="btn btn-primary">CREAR NUEVO REGISTRO </a>
				</article>
			{!! Form::close() !!}
			<article class="form-group">
				<table class="table table-bordered">
					<tr>
						<th>fecha</th>
						<th>hora de inicio</th>
						<th>hora  de finalizacion</th>
						<th>proyecto</th>
						<th>asunto</th>
						<th>asistentes</th>
						<th>asistentes externos</th>
						<th>elaborado por</th>
						<th>proxima reunion</th>
						<th>descripcion reunion</th>
						
					</tr>
					<tbody>

						@foreach($actareunions as $actareunion)
							<tr>
								<td>{{ $actareunion->fecha}}</td>
								<td>{{ $actareunion->horainicio}}</td>
								<td>{{ $actareunion->horafinal}}</td>
								<td>{{ $actareunion->lugar}}</td>
								<td>{{ $actareunion->proyecto}}</td>
								<td>{{ $actareunion->asunto}}</td>
								<td>{{ $actareunion->asistentes}}</td>
								<td>{{ $actareunion->asistentesexternos}}</td>
								<td>{{ $actareunion->elaboradopor}}</td>
								<td>{{ $actareunion->proximareunion}}</td>
								<td>{{ $actareunion->descripionreunion}}</td>
								
								<td>
									<a class="btn btn-primary btn-xs" href="{{ route('actareunion.edit',['id' => $actareunion->id] )}}" >EDITAR</a>
									<a class="btn btn-danger btn-xs" href="{{ route('actareunion/destroy',['id' => $actareunion->id] )}}">ARCHIVAR</a>
								</td>
							</tr>
						@endforeach

					</tbody>
				</table>
			</article>
		</section>
	</section>
@endsection
