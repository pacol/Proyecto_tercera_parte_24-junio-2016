<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Aprendiz extends Model
{
    protected $table = 'aprendizs';
  protected $fillable = ['nombre', 'apellido', 'documento', 'edad', 'curso', 'correo'];
  protected $guarded = ['id'];
}
