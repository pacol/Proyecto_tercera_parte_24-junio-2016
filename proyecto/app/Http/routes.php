<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::get('/home', 'HomeController@index');
//rutas para el recurso Aprendiz
Route::resource('aprendiz','AprendizController');
//una nueva ruta para eliminar registros con el metodo get
Route::get('aprendiz/destroy/{id}', ['as' => 'aprendiz/destroy', 'uses'=>'AprendizController@destroy']);
//ruta para realizar busqueda de registros.
Route::post('aprendiz/search', ['as' => 'aprendiz/search', 'uses'=>'AprendizController@search']);
//ruta para realizar actualizacion de registros.
Route::post('aprendiz/update', ['as' => 'aprendiz/update', 'uses'=>'AprendizController@update']);
//rutas para el recurso Profesor
Route::resource('profesor','ProfesorController');
//una nueva ruta para eliminar registros con el metodo get
Route::get('profesor/destroy/{id}', ['as' => 'profesor/destroy', 'uses'=>'ProfesorController@destroy']);
//ruta para realizar busqueda de registros.
Route::post('profesor/search', ['as' => 'profesor/search', 'uses'=>'ProfesorController@search']);
//ruta para realizar actualizacion de registros.
Route::post('profesor/update', ['as' => 'profesor/update', 'uses'=>'ProfesorController@update']);
//rutas para el recurso Acta reunion
Route::resource('actareunion','ActareunionController');
//una nueva ruta para eliminar registros con el metodo get
Route::get('actareunion/destroy/{id}', ['as' => 'actareunion/destroy', 'uses'=>'ActareunionController@destroy']);
//ruta para realizar busqueda de registros.
Route::post('actareunion/search', ['as' => 'actareunion/search', 'uses'=>'ActareunionController@search']);
//ruta para realizar actualizacion de registros.
Route::post('actareunion/update', ['as' => 'actareunion/update', 'uses'=>'ActareunionController@update']);
