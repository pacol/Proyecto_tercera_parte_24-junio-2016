<?php

namespace App\Http\Controllers;

use App\Models\Profesor as Profesor;

use Illuminate\Http\Request;

use App\Http\Requests;

class ProfesorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $profesors = Profesor::all();
        return \View::make('profesor/list',compact('profesors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return \View::make('profesor/new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $profesor = new Profesor;
       $profesor->nombre = $request->nombre;
       $profesor->apellido = $request->apellido;
       $profesor->documento = $request->documento;
       $profesor->genero = $request->genero;
       $profesor->save();
       return redirect('profesor');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $profesor = Profesor::find($id);
        return \View::make('profesor/update',compact('profesor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $profesor = Profesor::find($request->id);
        $profesor->nombre = $request->nombre;
        $profesor->apellido = $request->apellido;
        $profesor->documento = $request->documento;
        $profesor->genero = $request->genero;
        $profesor->save();
        return redirect('profesor');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $profesor = Profesor::find($id);
        $profesor->delete();
        return redirect()->back();
    }
    public function search(Request $request)
    {

    
      $profesors = Profesor::where('nombre','=',$request->nombre)       
        ->get();
       return \View::make('profesor/list',compact('profesors'));
   }
}
