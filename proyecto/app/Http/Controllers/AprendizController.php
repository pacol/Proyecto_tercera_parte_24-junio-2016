<?php

namespace App\Http\Controllers;

use App\Models\Aprendiz as Aprendiz;

use Illuminate\Http\Request;

use App\Http\Requests;

class AprendizController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $aprendizs = Aprendiz::all();
        return \View::make('aprendiz/list',compact('aprendizs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return \View::make('aprendiz/new');
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $aprendiz = new Aprendiz;
       $aprendiz->nombre = $request->nombre;
       $aprendiz->apellido = $request->apellido;
       $aprendiz->documento = $request->documento;
       $aprendiz->edad = $request->edad;
       $aprendiz->curso = $request->curso;
       $aprendiz->correo = $request->correo;
       $aprendiz->save();
       return redirect('aprendiz');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $aprendiz = Aprendiz::find($id);
        return \View::make('aprendiz/update',compact('aprendiz'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $aprendiz = Aprendiz::find($request->id);
        $aprendiz->nombre = $request->nombre;
        $aprendiz->apellido = $request->apellido;
        $aprendiz->documento = $request->documento;
        $aprendiz->edad = $request->edad;
        $aprendiz->curso = $request->curso;
        $aprendiz->correo = $request->correo;
        $aprendiz->save();
        return redirect('aprendiz');
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $aprendiz = Aprendiz::find($id);
        $aprendiz->delete();
        return redirect()->back();
    }
     public function search(Request $request)
    {

    
      $aprendizs = Aprendiz::where('nombre','=',$request->nombre)       
        ->get();
       return \View::make('aprendiz/list',compact('aprendizs'));
    }
}
