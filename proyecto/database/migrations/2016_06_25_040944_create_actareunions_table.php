<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActareunionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actareunions', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha');
            $table->string('horainicio');
            $table->string('horafinal');
            $table->string('lugar');
            $table->string('proyecto');
            $table->string('asunto');
            $table->string('asistentes');
            $table->string('asistentesexternos');
            $table->string('elaboradopor');
            $table->string('proximareunion');
            $table->string('descripcionreunion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('actareunions');
    }
}
